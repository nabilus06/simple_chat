const express = require('express');
const app = express();
const http = require('http').Server(app);
const path = require('path');
const favicon = require('serve-favicon');
const socketio = require('socket.io')(http);
const { execFile } = require('child_process');
const he = require('he'); // Bibliothèque pour l'échappement HTML ///

app.use(express.static(path.join(__dirname, 'static')));
app.use(favicon(path.join(__dirname, 'static', 'images', 'favicon.ico')));

http.listen(process.env.PORT || 5000, () => {
  console.log(`Server running at http://127.0.0.1:${process.env.PORT || 5000}`);
});

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
});

socketio.on('connection', (socket) => {
  socket.on('message', (msg) => {
    // Échappez les données avant de les traiter
    const escapedMsg = he.encode(msg);

    if (escapedMsg.startsWith('/date')) {
      const cmd = escapedMsg.substring('/date'.length).trim();
      if (cmd) {
        const command = 'date';
        const args = [];

        execFile(command, args, (error, stdout, stderr) => {
          if (error) {
            console.error('Error executing command:', error.message);
            socketio.emit('message', 'Error executing command');
            return;
          }
          // Échappez la sortie avant de l'envoyer
          const escapedResponse = he.encode(stdout.toString());
          socketio.emit('message', escapedResponse);
        });
      } else {
        socketio.emit('message', 'Invalid command');
      }
    } else {
      // Échappez le message avant de l'envoyer
      socketio.emit('message', escapedMsg);
    }
  });
});
